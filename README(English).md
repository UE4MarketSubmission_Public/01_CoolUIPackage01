# CoolUI Package 01 Guide #
Open Unreal 4’s edit menu and select plugins.Then search for CoolUIPak01 and find the CoolUIPak01 plugin.Check the enabled option to make sure the CoolUIPak01 plugin is available.


![Add plugin to current project](README.md.resources/01_Images/Figure1.png)      
Figure1  Add plugin to current project     

Right click the mouse to create a new widget blueprint in the content browser and double-click to open it.In dashboard,you can search the widget name in the CoolUIPak01 plugin package and then drag it into the widget blueprint to complete the creation of the corresponding widget.  

Next,I will explain in detail the configuration and use of the three widgets contained in the CoolUIPak01 plugin package.    
   
**1.ProninentAnimatedVerticalMenu**    
This widget has a total of three parameters,namely StartNum,SlotNum and MenuSlotInfo,as shown in Figure 2.

![Add plugin to current project](README.md.resources/01_Images/Figure2.png)     
Figure 2 Configuration parameters of ProninentAnimatedVerticalMenu   

**StartNum** is used to define the index value of the first menu item, which is 1 by default and can be modified by the user to any non-negative integer.    
**Slot Num** indicates the number of menu items in the menu. User can set it to any positive integer according to actual needs. If set to 0, the menu will not display any content.   
 
The **MenuSlotInfo** parameter is an array that is used to configure the information for each menu item. The specific configurable items are shown in Figure 3.  
  
![Add plugin to current project](README.md.resources/01_Images/Figure3.png)    
Figure 3 Configuration parameters of MenuSlotInfo  

**MenuNumber** is used to configure the leftmost number of a menu item, which can be left blank if it is not needed.    
**MenuName** is used to set the name of menu item.    
**SlotClass** is used to configure the menu item's class reference.The CoolUIPak01 plugin has built-in a widget named WBP_ProminengtAnimatedMenuVerticalSlot. You can use this option to set SlotClass directly.    
**SlotConfig** is used to configure the menu style. The specific configurable items are shown in Figure 4.   

![Add plugin to current project](README.md.resources/01_Images/Figure4.png)   
Figure 4 Configuration parameters of SlotConfig    

**MenuNumberColor** is used to configure the left-most number color of the menu item, which is not required if the previous MenuNumber parameter is set to null.    
**MenuNumberFont** is used to configure the left-most number font of the menu item. If the previous MenuNumber parameter is set to null, you do not need to configure this parameter.    
**MenuNumberRenderTransform** is used to configure the left-most number  of the menu item,including translation,scale,shear and angle. If the previous MenuNumber parameter is set to null, you do not need to configure this parameter.    

**ButtonStyle** is used to configure the style of menu item button.   
**ButtonRenderTransform** is used to configure the menu item button's translation,scale,shear and angle.   
**MenuNameColor** is used to configure the color of menu item.    
**MenuNameFont** is used to configure the font of menu item.    
**MenuNameRenderTransform** is used to configure the menu item's translation,scale,shear and angle.    

**2.HoverToDisplayMenuNameVerticalMenu**   
This widget parameters and parameters of the ProninentAnimatedMenuVerticalMenu widget basically the same, as shown in Figure 5.

![Add plugin to current project](README.md.resources/01_Images/Figure5.png)   
Figure 5 Configuration parameters of HoverToDisplayMenuNameVerticalMenu  
  
The **StartNum** and **SlotNum** parameters are configured as those in the ProninentAnimatedMenuVerticalMenu widget.  
**MenuSlotInfo** is also used to configure menu items. Figure 6 shows the configuration items.   

![Add plugin to current project](README.md.resources/01_Images/Figure6.png)   
Figure 6 MenuSlotInfo parameters configurable items    

**MenuName** is used to configure menu name.    
**MenuSlotClass** is used to configure the menu item's class reference.The CoolUIPak01 plugin has built-in a widget named WBP_HoverToDisplayMenuNameVerticalMenuSlot. You can use this option to set MenuSlotClass directly.    
**MenuSlotStyle** is used to configure the style of the menu item. Figure 7 shows the configuration items.   

![Add plugin to current project](README.md.resources/01_Images/Figure7.png)   
Figure 7 MenuSlotStyle parameters configurable items    

**MenuNameColor** is used to configure the color of menu item’s name.    
**MenuNameFont** is used to configure the font of menu item’s name.    
**MenuNameJustification** is used to configure the justification of menu item’s name.    
**MenuNameRenderTransform** is used to configure the translation,scale,shear,angle of the menu item’s name.  
**MenuButtonNormalStyle** is used to configure the style of the menu button under normal conditions.    
**MenuButtonPressStyle** is used to configure the style of the menu button under press conditions.    
**MenuButtonRenderTransform** is used to configure the translation,scale,shear,angle of the menu button.    
**MenuIconImage** is used to configure the image of menu item under normal conditions.    
**MenuIconPressImage** is used to configure the image of menu item under press conditions.    
**MenuIconTransform** is used to configure the translation,scale,shear,angle of the menu image.    
**MenuIconHoveredScale** is used to configure the scale of the menu icon when menu item is hovered.        

**3.ClickableSlider**    
The widget has only one parameter which named SliderConfig, the specific configuration items are as follows:    

![Add plugin to current project](README.md.resources/01_Images/Figure8.png)     
Figure 8 ClickableSlider configuration items    

**InitialStep** configures the step in the initial state.    
**TotalStep** is the total number of steps for configuring the slider.    
**Background** is set the background of this widget.    
**LastButtonImage** is used to set the image of last button.    
**LastButtonRenderAngle** is used to set the angle of the last button’s image.    
**NextButtonImage** is used to set the image of next button.    
**NextButtonRenderAngle** is used to set the angle of the next button’s image.    
**ButtonMargins** configures the margin of next button and last button.    
**SliderBarImage** configures the slider bar image.    
**SliderThumbImage** configures the slider thumb image.    

