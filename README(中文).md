# CoolUI Package 01 操作指南 #
打开虚幻4的编辑菜单，选择plugins，搜索CoolUIPak01，找到CoolUIPak01插件，将Enabled选项勾上，确保CoolUIPak01插件可用。


![Add plugin to current project](README.md.resources/01_Images/Figure1.png)      
图1 添加插件到项目中  

接下来在内容浏览器的空白处右击鼠标新建一个控件蓝图，双击打开，在控制板中搜索插件包中的插件名称，然后将其拖入控件蓝图中即可完成相应控件的创建。    
接下来详细讲解CoolUIPak01插件包中所包含的三个控件的配置及使用。    

**1.ProninentAnimatedVerticalMenu**    
该控件一共有三个参数，分别是StartNum、SlotNum以及MenuSlotInfo，如图2所示

![Add plugin to current project](README.md.resources/01_Images/Figure2.png)     
图2 ProninentAnimatedMenuVerticalMenu控件配置参数    

StartNum用于定义第一个菜单项的索引值，默认情况下为1，用户可将其修改为任何非负的整数。   
SlotNum表示菜单当中菜单项的个数，用户可以根据实际需要将其设定为任意正整数，如果设置为0，该菜单将不会显示任何内容。    
MenuSLotInfo参数是一个数组，用于配置每一个菜单项的信息，具体可配置项如图3所示。      

![Add plugin to current project](README.md.resources/01_Images/Figure3.png)    
图3 MenuSlotInfo参数可配置项   

MenuNumber用于配置菜单项最左边的编号，如果不需要显示该项，可将其置为空。    
MenuName用于配置该菜单项的名称。    
SlotClass用于配置菜单项的类引用，该插件中已经内置了一个名为WBP_ProminengtAnimatedMenuVerticalSlot的控件，配置的时候直接选择该选项即可。    
SlotConfig用于对菜单的样式进行配置，具体可配置项如图4所示。    

![Add plugin to current project](README.md.resources/01_Images/Figure4.png)   
图4 SlotConfig参数可配置项      

MenuNumberColor用于配置菜单项最左边的编号的颜色，如果之前的MenuNumber参数设置为空，可不用配置该参数。    
MenuNumberFont用于配置菜单项最左边的编号的字体，如果之前的MenuNumber参数设置为空，可不用配置该参数。    
MenuNumberRenderTransfor用于配置菜单项最左边的编号的位置、大小、剪切值、旋转角度等信息，如果之前的MenuNumber参数设置为空，可不用配置该参数。    
ButtonStyle用于配置菜单项按钮的样式。    
ButtonRenderTransform用于配置菜单项按钮的位置、大小、剪切值、旋转角度等信息。    
MenuNameColor用于配置菜单名称的颜色。    
MenuNameFont用于配置菜单名称的字体。    
MenuNameRenderTransform用于配置菜单名称的位置、大小、剪切值、旋转角度等信息。    
MenuSlotInfo这个参数的数组成员数量应与SlotNum参数的值一致。    

**2.HoverToDisplayMenuNameVerticalMenu**   
该控件的参数与ProninentAnimatedMenuVerticalMenu控件的参数大同小异，如图5   
 
![Add plugin to current project](README.md.resources/01_Images/Figure5.png)   
图5 HoverToDisplayMenuNameVerticalMenu控件配置参数      

StartNum和SlotNum参数配置同ProninentAnimatedMenuVerticalMenu控件中的一致。    
MenuSlotInfo同样用于对菜单项进行配置，具体配置项如图6    

![Add plugin to current project](README.md.resources/01_Images/Figure6.png)   
图6 MenuSlotInfo 参数可配置项    

MenuName用于配置菜单的名称。    
MenuSlotClass用于配置菜单项的类引用，该插件中已经内置了一个名为WBP_ HoverToDisplayMenuNameVerticalMenuSlot的控件，配置的时候直接选择该选项即可。    
MenuSlotStyle用于对菜单项的样式进行配置，具体配置项如图7    

![Add plugin to current project](README.md.resources/01_Images/Figure7.png)   
图7 MenuSlotStyle 参数可配置项       

MenuNameColor配置菜单名的颜色。    
MenuNameFont配置菜单名的字体。    
MenuNameJustification配置菜单名的对齐方式。    
MenuButtonRenderTransform配置菜单名的位置、大小、剪切值、旋转角度等信息。    
MenuButtonNormalStyle配置正常状态下菜单按钮的样式。    
MenuButtonPressStyle配置点击状态下菜单按钮的样式。    
MenuButtonRenderTransform配置菜单项按钮的位置、大小、剪切值、旋转角度等信息。    
MenuIconImage配置正常状态下菜单的图片。    
MenuIconPressImage配置点击状态下菜单的图片。    
MenuIconTransform配置菜单图标的位置、大小、剪切值、旋转角度等信息。    
MenuIconHoveredScale配置菜单处于悬停状态时，图标的缩放比例。    

**3.ClickableSlider**    
该控件只有一个名为SliderConfig的参数，具体配置项如下：   
 
![Add plugin to current project](README.md.resources/01_Images/Figure8.png)     
图8 ClickableSlider配置项      
  
InitialStep配置初始状态下的步骤。    
TotalStep配置滑动条的总步骤数。    
Background配置滑动条的背景。    
LastButtonImage配置下一步按钮的图片。    
LastButtonRenderAngle配置下一步按钮图片的旋转角度。    
NextButtonImage配置上一步按钮的图片。    
NextButtonRenderAngel配置上一步按钮图片的旋转角度。    
ButtonMargins配置上一步和下一步按钮的边距。    
SliderBarImage配置滑动条的图片。    
SliderThumbImage配置滑动条上点的图片。    

